#!/bin/bash
set -e

while getopts ":d:f:p:l:a" option; do
  case "${option}" in
    d)
        set -x
        ;;
    f)
        XCFFILE=${OPTARG}
        ;;
    p)
        PYFILE=${OPTARG}
        ;;
    l)
        I18N=${OPTARG}
        ;;
    a)
        ALL_I18N=true
        ;;
  esac
done

if [[ -z "$XCFFILE" || -z "$PYFILE" || -z "$I18N" && -z "$ALL_I18N" ]]; then
    echo "usage: `basename $0` [-d] -p <pythonFuFile> -f <xcfFile> -l <language> [-a]"
    exit 1
fi

function process {
    # Start gimp with python-fu batch-interpreter
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        gimp -i --batch-interpreter=python-fu-eval -b "$(cat ${PYFILE})"
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        /Applications/GIMP.app/Contents/MacOS/gimp -i --batch-interpreter=python-fu-eval -b "$(cat ${PYFILE})"
    fi
}

# use env to pass params
export XCFFILE
languageDir=languages

if [ "$ALL_I18N" = true ]
# if [ "$I18N_ALL" = true ];
then
    for d in ${languageDir}/*;do
        export I18N=$(basename $d)
        process
    done
else
    export I18N
    process
fi
